### Affected version

<!--
Provide at least the following information:
* Your OS and version
* Affected Application version
-->

### Bug summary

<!-- 
Provide a short summary of the bug you encountered.
-->

### Steps to reproduce

<!-- 
1. Step one
2. Step two
3. ...
-->

### What happened

<!-- 
What did XESTOS do that was unexpected?
-->

### What did you expect to happen

<!-- 
What did you expect XESTOS to do?
-->

### Relevant logs, screenshots, screencasts etc.

<!-- 
If you have further information, such as technical documentation, logs,
screenshots or screencasts related, please provide them here.

If the bug is a crash, please obtain a stack trace
-->


<!-- Do not remove the following line. -->
/label ~"1. Bug"
